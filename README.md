# Stocks coding challenge

## How to run the application

There are two apps: `stocks` and `stocks-api`.

- `stocks` is the front-end. It uses Angular 7 and Material. You can run this using `yarn serve:stocks`
- `stocks-api` uses Hapi and has a very minimal implementation. You can start the API server with `yarn serve:stocks-api`

A proxy has been set up in `stocks` to proxy calls to `locahost:3333` which is the port that the Hapi server listens on.

> You need to register for a token here: https://iexcloud.io/cloud-login#/register/ Use this token in the `environment.ts` file for the `stocks` app.

> The charting library is the Google charts API: https://developers.google.com/chart/

## Problem statement

[Original problem statement](https://bitbucket.org/kburson3/developer-puzzles/src/3fb1841175cd567a63abfbe18c08e4d2a734c2e9/puzzles/web-api/stock-broker.md)

### Task 1

Please provide a short code review of the base `master` branch:

1. What is done well?
2. What would you change?
3. Are there any code smells or problematic implementations?

Chris's Answers

1. What is done well?
- The modularity of this project is done well. Seperating the concerns and then having distinct folders for shared vs. individual components is a skill I like to practice.
- Start up of project is done well. Easy to ng serve the web server.
- Seperating the API from the UI frontend is always a good idea. Well done.

2. What would you change?
- I would like to set up some routing. Its always a good idea to use routing baseline because all things scale out. Good practice to get into.
- The main module should be at root level.
- The Chart in the UI for chart component is not rendering because of a syntax error. This i will fix in my PR
- Make a better User Experience. Few things to do to have a better User Interface. Small changes can make big differences.

3. Are there any code smells or problematic implementations?
- The UI. Needs some sprucing up.
- The chart component is syntatically incorrect, thus not rendering. Will fix.



> Make a PR to fix at least one of the issues that you identify

### Task 2

```
Business requirement: As a user I should be able to type into
the symbol field and make a valid time-frame selection so that
the graph is refreshed automatically without needing to click a button.
```

_**Make a PR from the branch `feat_stock_typeahead` to `master` and provide a code review on this PR**_

> Add comments to the PR. Focus on all items that you can see - this is a hypothetical example but let's treat it as a critical application. Then present these changes as another commit on the PR.

### Task 3

```
Business requirement: As a user I want to choose custom dates
so that I can view the trends within a specific period of time.
```

_**Implement this feature and make a PR from the branch `feat_custom_dates` to `master`.**_

> Use the material date-picker component

> We need two date-pickers: "from" and "to". The date-pickers should not allow selection of dates after the current day. "to" cannot be before "from" (selecting an invalid range should make both dates the same value)

### Task 4

```
Technical requirement: the server `stocks-api` should be used as a proxy
to make calls. Calls should be cached in memory to avoid querying for the
same data. If a query is not in cache we should call-through to the API.
```

_**Implement the solution and make a PR from the branch `feat_proxy_server` to `master`**_

> It is important to get the implementation working before trying to organize and clean it up.
